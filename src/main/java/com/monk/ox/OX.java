/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monk.ox;

import java.util.Scanner;

/**
 *
 * @author acer
 */
public class OX {

    static char winner = '-';
    static boolean isFinish = false;
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    static char player = 'X';

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void showTable() {
        System.out.println("  1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println("");

        }
        System.out.println("");
    }

    static void showTurn() {
        System.out.println(player + " Turn");
    }

    static void input() {
        while (true) {
            System.out.println("Please input Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;

            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            }
            System.out.println("Error: This table is not available! ");
        }

    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkDiagonal() {

        if (table[2][2] != '-') {
            int n = 0;
            for (row = 0, col = 0; col < 2; col++, row++) {
                if (table[row][col] == table[row + 1][col + 1]) {
                    n++;
                    if (n == 2) {
                        winner = player;
                        isFinish = true;
                        return;
                    }
                }
            }
            n = 0;
            for (row = 2, col = 0; col < 2; col++, row--) {
                if (table[row][col] == table[row - 1][col + 1]) {
                    n++;
                    if (n == 2) {
                        winner = player;
                        isFinish = true;
                        return;
                    }
                }
            }
        }
    }

    static void checkDraw() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (table[row][col] == '-') {
                    return;
                }
            }
        }
        isFinish = true;
    }

    static void checkWin() {
        checkRow();
        checkCol();
        checkDiagonal();
        checkDraw();
    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void showResult() {
        if (winner == '-') {
            System.out.println("Draw!!");
        } else {
            System.out.println(winner + " win!!");
        }
    }

    static void showBye() {
        System.out.println("Bye bye");
    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
        } while (!isFinish);
        showTable();
        showResult();
        showBye();

    }
}
